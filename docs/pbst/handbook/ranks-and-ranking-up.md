# Points and ranking up

## How to earn points
Points can be earned in a few ways.

**_Trainings:_**

One way, and the most common way, to earn points is by attending a training. These trainings get announced on both the PBST discord as well as on our group wall.  Trainings can be hosted by:
 * Trainers,
 * Tier 4 Special Defense.

When attending a training, you can get anywhere between 0-5 points. How much points you receive depends on your performance and behaviour throughout the training. Different hosts has different ways to determine how well one does during a training.

Anyone is able to assist in a training, but it is up to the host on who will be an assistant. **Please do not ask to assist, this will simply lower your chances of you asssisting**.

For some trainings, such as mega trainings, you can get up to 10 points, depending on the same prinipals as normal trainings. To find out more about training, [click here](.../handbook/#training).

---

**_TMS Raids:_**

Another way to earn points is by attending a TMS raid. These raids get announced on the PBST discord. This is the best way to earn points. You will earn 5 points if you are in the PBST team for the entire raid, helping to defend the core against TMS. If you do exceptionally well, in most cases you get a bonus point which is logged by a trainer. 

To find out more about TMS, scroll down as well as [clicking here](handbook/#the-mayhem-syndicate).

---

**_Self Training:_**

The last way to earn points is by doing self training. Self training allows you to practice your skills in a specific activity. For all activities, there are 5 different levels. The higher the level, the higher the points.

 To find out more about self training, [click here](#self-trainings).


:::danger A change to ST:
As of the 27th of June 2020, **self-training is locked to Tier 1+ only.**
![img](https://cdn.discordapp.com/attachments/726547736689770536/726547788854460501/unknown.png) 
:::

---

## Practice Trainings

Practice Training, also known as PTs, can be hosted by any PBST member at any facility (preferably PBSTAC or PBSTTF). 
These Trainings are *unofficial* and you won’t *earn Points in any way*. 

Normal PTs will usually consist of a few attendees with a Tier or Cadet hosting it. 
Before a PT can start, the person hosting the PT **must state that it’s not an official training and must explain that you won’t earn any points in any way.** 

You are not allowed to host a PT 30 minutes before an official Training (or 15 minutes with permission from the host).

This means that a PT must end around 15 to 30 minutes before an actual training takes place.

If a person claims to be a real trainer or tier 4 SD, take a screenshot and report the user to a HR.

---

## Ranking up

Once you reach certain amount of points, you'll be able to rank up. Below are the different ranks and how much points you need for each.

*Cadet - 0 points required (automatically earned).*

*Tier 1 - 100 points required.*

*Tier 2 - 250 points required.*

*Tier 3 - 500 points required.*

*Tier 4 Special Defense - 800 points required.*

**You'll need to do an evaluation for both tier 1 and tier 4 special defense.** For tier 1 evaluations, you'll need to complete both a quiz section, as well as a patrol section in order to earn tier 1. For tier 4 SD evaluations, you'll need to host an evaluated training.

 For more information on both evaluations, please scroll below.


:::danger Keep in mind!
When you're a ``tier 1+``, you may be punished harder for mistakes you commit, you're supposed to be a role model for all the cadets and visitors in **Pinewood Facilities**. 
:::

---

## Tier 1 and Tier 4 SD's evaluation

There are two different tier evaluation. This section explains both in a clear way.

**_Tier 1 evaluation_**

Once you reach 100 points, you must participate in an evaluation if you want to get the **Tier 1** rank. 
It is not recommended to get more points before your evaluation, if you take the evaluation while having more than *150 points **you’ll be set back** to 150*.

Scheduled Tier evaluations can be found on the PBST schedule at [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility). 


There is a specialized server for Tier evaluations, use the command ``!pbstevalserver`` to get there. This only works if you have 100+ points.

A Tier 1 evaluation consists of 2 parts: a quiz and a test on patrolling skills. The training rules will be heavily enforced in this evaluation, not following them will result in an immediate fail. If you pass one part but fail the other, you may try again at another evaluation and skip the part you passed before. (There is a database where we log all the people who failed and passed the quiz or the patrol).

During the quiz, you will receive 8 questions about various topics including this handbook and training rules. The questions will vary in difficulty, some are easy but others require more thinking. You need to score at least 5/8 to pass. Answering must be done privately through whisper chat or a Private Message system sent by the host.

During the patrol test, you will receive Tier 1 loadouts and be tested on skills like your abilities in combat, teamwork, and following the handbook correctly. A number of **Tier 3**'s will work against you in this part, as they will try to bring the core to melt- or freezedown.

---


**_Tier 4 Special Defense evaluation_**

Tier 3s who reach 800 points are eligible for an SD evaluation to become Tier 4 special defense. If chosen, you are tasked to host a PBST Training and your performance will be closely watched. If you pass, you are given the title “``Passed SD Eval``” until the Trainers choose to promote you to Tier 4 4 special defense. Having Discord is required.

As a Tier 4 special defense, you receive the ability to place a KoS in Pinewood facilities, you can order room restrictions, and you don’t have to wear a uniform anymore. You will also receive Kronos mod at Pinewood’s training facilities, this is to be used **responsibly**.

Tier 4 SDs can host trainings with permission from a Trainer. They may host 6 times per week (Mega’s not included), with a maximum of 2 per day. Tier 4 SDs may not request a training more than 3 days ahead of time. There also has to be a 1-hour gap at least between the end of one training and the start of another.

---

## Trainer
To become a Trainer, all the current Trainers have to vote on your promotion. Only Tier 4 SDs are eligible for this promotion.

Trainers can host trainings without restrictions. Trainers are also responsible for some of PBST’s more administrative tasks, like points logging and promotions

:::danger Do not ask for promotions
We can't say it enough, do not ask for points, promotions or anything related to this. You are to earn points like everyone does, and these points define what rank you will be in the group. Trainers and SDs have the authority to deduct your points if they deem it necessary. Wait for the points to be logged, and earn your rank like everyone else.
:::

---

## When will points be logged?
Points doesn't get automatically logged. Each point is logged manually by a trainer. Points being logged depends on which trainer is availible and has the time to log points. **DO NOT ask trainers for points to be logged**. This will only be annoying for the trainers logging the points in question. By spamming the trainers about when points will be logged, ***you're only putting yourself in trouble.*** They have the ability to **SUBTRACT** your points.


**If you have any further questions and enquiries about this section, feel free to ask in the PBST discord on the #handbook-questions channel. (*DISCLAIMER: You must be 13 or over*).**

