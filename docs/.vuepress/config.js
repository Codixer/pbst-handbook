module.exports = ctx => ({
  title: 'PBST Handbook',
  description: 'The official PBST Handbook',
  themeConfig: {
    repo: 'https://gitlab.com/pinewood-builders/pbst-handbook',
    editLinks: true,
    docsDir: 'docs/',
    logo: '/PBST-Logo.png',
    smoothScroll: true,
    sidebarDepth: 3,
    yuu: {
      defaultDarkTheme: true,
    },
    algolia: ctx.isProd ? ({
      apiKey: '335b94bd05315a8ed572b77d95e6d5b7',
      indexName: 'pbst'
    }) : null,

    nav: [
      {
        text: 'Home',
        link: '/'
      },
      {
        text: 'PBST Handbook',
        link: '/pbst/handbook/handbook.html'
      },
      {
        text: 'Credits',
        link: '/credits/'
      },
      {
        text: 'Pinewood',
        items: [{
          text: 'PET-Handbook',
          link: 'https://pet.pinewood-builders.com'
        },
        {
          text: 'TMS-Handbook',
          link: 'https://tms.pinewood-builders.com'
        }
        ]
      }
    ],

    sidebar: [{
      collapsable: true,
      title: '👮 PBST Handbook',
      children: ['/pbst/handbook/handbook', '/pbst/handbook/ranks-and-ranking-up'],
    },
    /* {
      collapsable: true,
      title: '🔍 Points of intrest',
      children: ['/pbst/poi/pbcc'],
    },  */
    
    {
      collapsable: true,
      title: '👷‍♂️ Community Pages',
      children: ['/pbst/cp/combat-tips', '/pbst/cp/special-commands'],
    },
    ],
    
  },
  head: [
    ['link', {
      rel: 'icon',
      href: '/PBST-Logo.png'
    }],
    ['link', {
      rel: 'manifest',
      href: '/manifest.json'
    }],
    ['meta', {
      name: 'theme-color',
      content: '#3eaf7c'
    }]
  ],
  plugins: [
    [
      'vuepress-plugin-zooming',
      {
        selector: '.theme-default-content img.zooming',
        delay: 1000,
        options: {
          bgColor: 'white',
          zIndex: 10000,
        },
      },
    ],
    ['@vuepress/pwa',
      {
        serviceWorker: true,
        updatePopup: true
      }
    ],
    [
      '@vuepress/google-analytics',
      {
        'ga': 'UA-168777162-2' // UA-00000000-0
      }
    ],
    ['vuepress-plugin-global-toc'],
    [
      'vuepress-plugin-copyright',
      {
        noCopy: true, // the selected text will be uncopiable
        minLength: 100, // if its length is greater than 100
      },
    ],
  ],
})